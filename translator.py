#!/usr/bin/env python3
import re
import json
from collections import namedtuple

RE_TITLE = re.compile(r'(?:[^=]*= )?(.*)')
RE_ENG_NAME = re.compile(
    r'(?:(?:S1|S3|Kerbodyne|LFB)\s+)?'
    r'(?P<model>[-+\w]+)\s+'
    r'(?:(?P<prealias>[^"]+)\s+)?'
    r'(?:"(?P<alias>[^"]+)"\s+)?'
    r'(?P<type>.*)'
)
RE_TANK_NAME = re.compile(
    r'(?:(?:Kerbodyne|Rockomax)\s+)?(?P<model>[-+\w]+'
    r'(?: (Cylindrified|Roundified))?).*'
)

FUEL_TYPE = {
    'liquidfuel': 'liquid',
    'turbine': 'jet',
    'solidbooster': 'solid',
    'electric': 'xenon',
    'nuclear': 'liquid',
    'monoprop': 'mono',
}


def _determine_engine_size(mass):
    if mass < 0.5:
        return 'tiny'
    elif mass < 2.5:
        return 'small'
    elif mass < 9:
        return 'large'
    else:
        return 'extralarge'


def _determine_engine_radius(mass):
    if mass < 0.5:
        return 0.625
    elif mass < 2.5:
        return 1.25
    elif mass < 9:
        return 2.5
    else:
        return 3.75


def _parse_atmo_curve(module):
    '''
    Returns ISP in (vacuum, atmosphere, jet)
    '''
    atmo_curve = module['atmosphereCurve']['key']
    if isinstance(atmo_curve[1], int):
        return (None, None, atmo_curve[1])
    atmo_curve = {
        x[0]: x[1]
        for x in atmo_curve
    }
    return (atmo_curve.get(0), atmo_curve.get(1), None)


def _determine_fuel_type(resources):
    types = set(resources.keys())
    if types == {'liquidfuel', 'oxidizer'}:
        return 'liquid'
    elif types == {'liquidfuel'}:
        # Skip liquid only.
        return None
    elif types == {'xenongas'}:
        return 'xenon'
    elif types == {'monopropellant'}:
        return 'mono'
    elif types == {'ore'}:
        return None
    else:
        raise ValueError(f'bad fuel types: {types!r}')


def _parse_engine_title(part_title):
    '''
    Returns (name, model, alias)
    '''
    title = RE_TITLE.match(part_title).group(1)
    if title == 'Launch Escape System':
        return title, title, 'LES', 'Launch-Escape'
    elif title == 'Sepratron I':
        return title, title, 'Sepra', 'Sepratron'
    name_match = RE_ENG_NAME.match(title)
    if name_match is None:
        return None, None, None, None
    model = name_match.group('model')
    alias = name_match.group('alias')
    prealias = name_match.group('prealias')
    if prealias == 'R.A.P.I.E.R.':
        alias = 'RAPIER'
    name = f'{model} {alias}'
    return title, name, model, alias


def _parse_tank_model(part_title):
    title = RE_TITLE.match(part_title).group(1)
    if (
        'Adapter' in title or
        title.startswith('Mk3') or
        title.startswith('Mk1') or
        title.endswith('Holding Tank') or
        title.endswith('Fuel Duct')
    ):
        return None, None
    match = RE_TANK_NAME.match(title)
    if match is None:
        return None, None
    model = match.group('model')
    model = {
        'Stratus-V Roundified': 'STRAT-V-RND',
        'Stratus-V Cylindrified': 'STRAT-V-CYL',
    }.get(model, model)
    return title, model


def _parse_common(part):
    if 'attachRules' not in part:
        return None
    can_stack, can_radial, _, _, _, *_ = part['attachRules']
    if can_stack and can_radial:
        mount = 'both'
    elif can_stack:
        mount = 'stack'
    elif can_radial:
        mount = 'radial'
    else:
        raise ValueError(f'failed to find mount rules for part:\n{part!r}')
    return {
        'category': part['category'].lower(),
        'cost': part['cost'],
        'mass': part['mass'],
        'mount': mount,
    }


def _parse_tank_size(part):
    sizes = set(part['bulkheadProfiles'].split(', '))
    if 'size0' in sizes:
        size = 'tiny'
    elif 'size1' in sizes:
        size = 'small'
    elif 'size2' in sizes:
        size = 'large'
    elif 'size3' in sizes:
        size = 'xlarge'
    elif 'srf' in sizes:
        size = 'radial'
    else:
        raise ValueError(f'unknown tank size: {sizes!r}')
    return size


class Tank(namedtuple('Tank', (
    'title', 'name', 'model', 'path', 'category', 'cost',
    'size', 'mount', 'fuel_type',
    'mass', 'wet_mass', 'liq_fuel', 'oxi_fuel', 'xenon_fuel', 'mono_fuel',
    'resources',
))):

    def __repr__(self):
        return (
            f'Tank({self.name}, size={self.size}, fuel_type={self.fuel_type}, '
            f'mount={self.mount}, dry_mass={self.mass}, '
            f'wet_mass={self.wet_mass})'
        )

    def serialize(self):
        return dict(self._asdict())

    @classmethod
    def parse(cls, path, part):
        common = _parse_common(part)
        if common is None:
            return None
        if common['category'] != 'fueltank':
            return None

        title = part.get('title')
        if title is None:
            return None
        title, model = _parse_tank_model(title)
        if title is None:
            return None

        size = _parse_tank_size(part)

        resources = part.get('RESOURCE', [])
        if not isinstance(resources, list):
            resources = [resources]
        resources = {
            x['name'].lower(): x['amount']
            for x in resources
        }
        if not resources:
            return None
        total_fuel = sum(resources.values())
        fuel_type = _determine_fuel_type(resources)
        if fuel_type is None:
            return None
        if fuel_type == 'liquid':
            wet_mass = round(common['mass'] + (total_fuel / 200), 6)
        elif fuel_type == 'mono':
            wet_mass = round(common['mass'] + (total_fuel / 250), 6)
        elif fuel_type == 'xenon':
            wet_mass = round(common['mass'] + (total_fuel / 10000), 6)
        name = f'{model} ({fuel_type})'
        return cls(
            title=title, name=name, model=model, path=path, size=size,
            fuel_type=fuel_type, wet_mass=wet_mass,
            resources=resources,
            liq_fuel=round(resources.get('liquidfuel', 0) / 200, 6),
            oxi_fuel=round(resources.get('oxidizer', 0) / 200, 6),
            xenon_fuel=round(resources.get('xenongas', 0) / 10000, 6),
            mono_fuel=round(resources.get('monopropellant', 0) / 250, 6),
            **common
        )

    def old(self):
        mount = self.size.title()
        if self.mount == 'radial':
            mount = 'Radial mounted'
        return {
            self.model: {
                'name': self.name,
                'mount': mount,
                'dry mass': self.mass,
                'oxidizer': round(self.oxi_fuel * 200, 6),
                'liquid fuel': round(self.liq_fuel * 200, 6),
                'mass': self.wet_mass,
                'fuel mass': round(self.wet_mass - self.mass, 6),
                'fuel': self.fuel_type,
            }
        }


class Engine(namedtuple('Engine', (
    'title', 'name', 'alias', 'model',
    'fuel', 'size', 'cost', 'mount', 'category', 'engine_type',
    'mass', 'thrust', 'isp_vac', 'isp_jet', 'isp_atm', 'path',
    'radius', 'resources',
))):

    def __repr__(self):
        return (
            f'Engine({self.name}, mass={self.mass}, thrust={self.thrust}, '
            f'fuel={self.fuel}, size={self.size}, cat={self.category}, '
            f'type={self.engine_type}, isp={self.isp_vac}:{self.isp_atm}:'
            f'{self.isp_jet}, mount={self.mount}, radius={self.radius})'
        )

    def serialize(self):
        return dict(self._asdict())

    @classmethod
    def parse(cls, path, part):
        if 'MODULE' not in part:
            return None
        common = _parse_common(part)
        if common is None:
            return None
        if common['category'] != 'engine':
            return None

        title = part.get('title')
        if title is None:
            return None
        title, name, model, alias = _parse_engine_title(title)
        if name is None:
            return None

        if 'MODULE' not in part:
            return None
        modules = part['MODULE']
        if not isinstance(modules, list):
            modules = [modules]
        modules = [
            x for x in modules
            if x['name'] in ('ModuleEngines', 'ModuleEnginesFX')
        ]
        if not modules:
            return None

        resources = part.get('RESOURCE', [])
        if not isinstance(resources, list):
            resources = [resources]
        resources = {
            x['name'].lower(): x['amount']
            for x in resources
        }

        engines = []
        size = _determine_engine_size(common['mass'])
        radius = _determine_engine_radius(common['mass'])
        for i, module in enumerate(modules):
            thrust = module['maxThrust']
            engine_type = module['EngineType'].lower()
            fuel = FUEL_TYPE[engine_type]
            isp_vac, isp_atm, isp_jet = _parse_atmo_curve(module)
            if len(modules) > 1:
                etitle, ename, emodel, ealias = (
                    f'{title}-mode{i}', f'{name}-mode{i}', f'{model}-m{i}',
                    f'{alias}-m{i}',
                )
            else:
                etitle, ename, emodel, ealias = title, name, model, alias
            engines.append(cls(
                title=etitle, name=ename, model=emodel, alias=ealias,
                path=path, engine_type=engine_type, thrust=thrust,
                isp_vac=isp_vac, isp_atm=isp_atm, isp_jet=isp_jet, fuel=fuel,
                size=size, radius=radius, resources=resources,
                **common
            ))

        return engines

    def old(self):
        mount = None
        if self.mount == 'radial':
            mount = 'Radial mounted'
        elif self.mass < 0.5:
            mount = 'Tiny'
        elif self.mass < 2.5:
            mount = 'Small'
        # elif < 9 Large, then Xlarge
        else:
            mount = 'Large'
        return {
            self.name: {
                'mass': self.mass,
                'name': self.title,
                'atm isp': self.isp_atm,
                'thrust': self.thrust,
                'mount': mount,
                'vac isp': self.isp_vac,
                'fuel': self.fuel,
            }
        }


def old(engines, tanks):
    results = {
        'tanks': {},
        'engines': {},
    }
    for tank in tanks:
        results['tanks'].update(tank.old())
    for eng in engines:
        results['engines'].update(eng.old())
    return results


def serialize(engines, tanks):
    results = {
        'tanks': [],
        'engines': [],
    }
    for tank in tanks:
        results['tanks'].append(tank.serialize())
    for eng in engines:
        results['engines'].append(eng.serialize())
    return results


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--parts', '-p', default='parts.json',
                        help='parts json file, default: %(default)s')
    parser.add_argument('--out', '-o', default='parts.json.out',
                        help='output json file, default: %(default)s')
    parser.add_argument('--out2', '-O', default='serialized.json',
                        help='output json file, default: %(default)s')
    args = parser.parse_args()

    with open(args.parts) as f:
        parts = json.load(f)

    engines = []
    tanks = []
    for path, d in parts.items():
        part = d.get('PART')
        if part is None:
            continue
        parsed_engines = Engine.parse(path, part)
        if parsed_engines is not None:
            engines.extend(parsed_engines)
            continue
        parsed_tank = Tank.parse(path, part)
        if parsed_tank is not None:
            tanks.append(parsed_tank)
            continue

    for eng in sorted(engines, key=lambda x: x.name):
        print(eng)
    for tank in sorted(tanks, key=lambda x: x.name):
        print(tank)
    results = old(engines, tanks)
    with open(args.out, 'w') as f:
        json.dump(results, f, indent=4)
    print(f'dumped to {args.out}')
    results = serialize(engines, tanks)
    with open(args.out2, 'w') as f:
        json.dump(results, f, indent=4)
    print(f'dumped to {args.out2}')


if __name__ == '__main__':
    main()
